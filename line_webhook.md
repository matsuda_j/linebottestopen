# Line Messaging API サンプル

## 概要
Line Messaging APIを利用して下記の内容を試してみました。

1. Line DevelopersでBotを作成する
1. リッチメニューをAPIで登録する
1. リッチメニューの画像をAPIで登録する
1. 以下のテンプレートメッセージをAPI経由でユーザーへ表示するプログラムを設置する
  1. ボタンテンプレート
  1. 確認テンプレート
  1. 画像カルーセルテンプレート
1. Botを友だち追加して、動作を確認する

## 1. Line DevelopersでBotを作成する
1. Line Developersへログインする
1. Messaging APIを押下
<img src="./markdown/img/line_developers01.png" width="300px" />
1. 「アイコン画像」「アプリ名」「アプリ説明文」などを入力する
<img src="./markdown/img/line_developers02.png" width="200px" />
1. 利用規約等に同意しアプリを作成する
<img src="./markdown/img/line_developers04.png" width="200px" />
1. 作成したアプリを選択する
<img src="./markdown/img/line_developers05.png" width="300px" />
1. 「メッセージ送受信設定」を変更
<img src="./markdown/img/line_developers07.png" width="300px" />
  1. アクセストークンの発行
  1. Webhook送信 => 利用する
  1. Webhook URLを利用する

## 2. リッチメニューをAPIで登録する
### 登録したリッチメッセージの概要
* ユーザーに表示するメニュー名は「リッチメニューテスト」
* 画像サイズは 2500x843px
* 押下できる領域は3箇所
 * areas.boundsで押下できる領域を設定
 * areas.actionで押下時のアクションを設定
* ボタン押下時はpostbackでサーバーへデータを送信する
* リッチメニューはデフォルトで表示する（"selected":true）

### 実行コマンド
```bash
curl -v -X POST https://api.line.me/v2/bot/richmenu \
-H 'Authorization: Bearer  {access token}' \
-H 'Content-Type:application/json' \
-d \
'{
  "size":{
      "width":2500,
      "height":843
  },
  "selected":true,
  "name":"richmenu003",
  "chatBarText":"リッチメニューテスト",
  "areas":[
      {
        "bounds":{
            "x":0,
            "y":0,
            "width":833,
            "height":843
        },
        "action":{
            "type":"postback",
            "data":"from=richmenu&request_type=button_tpl"
        }
      },
      {
        "bounds":{
            "x":834,
            "y":0,
            "width":833,
            "height":843
        },
        "action":{
            "type":"postback",
            "data":"from=richmenu&request_type=confirm_tpl"
        }
      },
      {
        "bounds":{
            "x":1667,
            "y":0,
            "width":833,
            "height":843
        },
        "action":{
            "type":"postback",
            "data":"from=richmenu&request_type=image_tpl"
        }
      }
  ]
}'
```

## レスポンス
登録したリッチメニューのIDが返却される
```bash
{"richMenuId":"richmenu-07ef93fddd4c4bc46b7011aa3fc2c7fd"}
```

## 3. リッチメニューの画像をAPIで登録する
先程取得した```richMenuId```を利用して画像を登録する。  
登録する画像のサイズは、リッチメッセージ登録時に設定した2500x843pxとする。

```
curl -v -X POST https://api.line.me/v2/bot/richmenu/{richMenuId}/content \
-H "Authorization: Bearer {access token}" \
-H "Content-Type: image/png" \
-T ./richmenu3.png
```
## 4. Botプログラムを設置する

### ファイル名：```line_webhook.php```

```php
<?php
/**
 * Line Messaging API を利用したテスト
 * リッチメニューは予め登録しておく
 * ユーザーへのリッチメニュー紐付けと解除をおこなう
 */
class lineBotApi
{
  // Line Developers管理画面上の「アクセストークン」
  private $access_token = "{access token}";
  // 事前に登録したリッチメニューのID
  private $richmenu_id  = "richmenu-07ef93fddd4c4bc46b7011aa3fc2c7fd";
  private $reply_token  = "";     // 返信に必要なトークン(リクエストデータに入る)
  private $user_id      = "";     // リクエストしてきたユーザーID
  private $type         = "";     // リクエストのタイプ 
  private $postback_arr = array();
  private $line_api_url_arr = array();
  
  public function __construct()
  {
    // Lineからはjsonで飛んでくる
    $response_arr = json_decode(file_get_contents('php://input'), true);
    if(count($response_arr) == 0)
    {
      http_response_code(404);
      exit();
    }
    // 利用するメッセージの抽出
    $this->setMessages($response_arr['events'][0]);
    // 抽出したIDなどを利用してAPIのURLを設定
    $this->setUrls();
  }
  
  /**
   * 利用するメッセージの抽出
   * @param [array] $messages_arr [Lineからリクエストされてきたデータ]
   */
  private function setMessages($messages_arr)
  {
    $this->reply_token = $messages_arr['replyToken'];
    $this->user_id     = $messages_arr['source']['userId'];
    $this->type        = $messages_arr['type'];
    if ($messages_arr['type'] == 'postback')
    {
      parse_str($messages_arr['postback']['data'], $this->postback_arr);
    }
  }
  
  /**
   * 抽出したIDなどを利用してAPIのURLを設定
   */
  private function setUrls()
  {
    $this->line_api_url_arr = array(
      'use_richmenu'  =>
       "https://api.line.me/v2/bot/user/{$this->user_id}
       /richmenu/{$this->richmenu_id}",
      'stop_richmenu' => "https://api.line.me/v2/bot/user/
      {$this->user_id}/richmenu",
      'reply_message' => "https://api.line.me/v2/bot/message/reply",
    );
  }
  
  /**
   * curlを利用したAPIリクエスト
   * @param  [string] $reply_api_url     [APIのURL]
   * @param  [string] $custom_request    [POSTやDELETEなど]
   * @param  array  $additional_header [API毎に異なる必要なヘッダー]
   * @param  string $send_data         [送信データ]
   * @return
   */
  private function callApi($reply_api_url, $custom_request, 
  $additional_header = [], $send_data = "", $return_transfer = false)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $reply_api_url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_request);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, $return_transfer);
    if($custom_request == 'POST')
    {
      curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
    }
    $header = array_merge(
      array(
        "Authorization: Bearer {$this->access_token}"), 
        $additional_header);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    return curl_exec($ch);
  }
  
  /**
   * メイン処理
   */
  public function lineBotAction()
  {
    // フォロー、ブロック解除された際
    if ($this->type == 'follow')
    {
      $this->setRichMenu();
    }
    // ブロックされた際
    else if ($this->type == 'unfollow')
    {
      $this->unsetRichMenu();
    }
    // postbackイベント
    else if ($this->type == 'postback')
    {
      // リッチメニューから送信された場合
      if($this->postback_arr['from'] == 'richmenu')
      {
        // ボタンTPL
        if ($this->postback_arr['request_type'] == 'button_tpl')
        {
          $this->sendButtonTpl();
        }
        // 確認TPL
        else if ($this->postback_arr['request_type'] == 'confirm_tpl')
        {
          $this->sendConfirmTpl();
        }
        // 画像カルーセル
        else if ($this->postback_arr['request_type'] == 'image_tpl')
        {
          $this->sendImageTpl();
        }
      }
      // 選択肢を押下した場合
      else if ($this->postback_arr['from'] == 'tpl_action')
      {
        $this->sendSelected();
      }
    }
  }
  /**
   * 画像カルーセルを送付
   */
  private function sendImageTpl()
  {
    $reply_message[] = array(
      "type"     => "template",
      "altText" => "画像カルーセルテスト",
      "template" => array(
        "type" => "image_carousel",
        "columns" => array(
          array(
            "imageUrl" => "https://line.allcate.jp/image/yahoo.png",
            "action"   => array(
                "type"  => "uri",
                "label" => "外部リンク",
                "uri"   => "https://yahoo.co.jp"
            )
          ),
          array(
            "imageUrl" => "https://line.allcate.jp/image/line.png",
            "action"   => array(
                "type"  => "uri",
                "label" => "外部リンク",
                "uri"   => "https://line.me/ja/"
            )
          ),
        )
      )
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $res = $this->callApi($this->line_api_url_arr['reply_message'], 
    "POST", $addadditional_header, json_encode($send_data), true);
  }
  /**
   * 選択肢が押下された際のアクション
   */
  private function sendSelected()
  {
    $response_tpl_messages = array(
      'select_a'   => '「選択肢A」が選択されました',
      'select_b'   => '「選択肢B」が選択されました',
      'select_c'   => '「選択肢C」が選択されました',
      'select_yes' => '「はい」が選択されました',
      'select_no'  => '「いいえ」が選択されました',
    );

    $reply_message[] = array(
      "text" => $response_tpl_messages[$this->postback_arr['selected']],
      "type" => "text",
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $this->callApi($this->line_api_url_arr['reply_message'], 
    "POST", $addadditional_header, json_encode($send_data));
  }
  /**
   * 確認TPL送付
   */
  private function sendConfirmTpl()
  {
    $reply_message[] = array(
      "type"     => "template",
      "altText" => "確認TPLテスト",
      "template" => array(
        "type" => "confirm",
        "text" => "【確認】〜してもよろしいですか？",
        "actions" => array(
          array(
            "type"  => "postback",
            "label" => "はい",
            "data"  => "from=tpl_action&selected=select_yes",
          ),
          array(
            "type"  => "postback",
            "label" => "いいえ",
            "data"  => "from=tpl_action&selected=select_no",
          ),
        )
      )
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $res = $this->callApi($this->line_api_url_arr['reply_message'], 
    "POST", $addadditional_header, json_encode($send_data), true);
  }
  /**
   * ボタンTPL送付
   */
  private function sendButtonTpl()
  {
    $reply_message[] = array(
      "type"     => "template",
      "altText" => "ボタンTPLテスト",
      "template" => array(
        "type" => "buttons",
        "thumbnailImageUrl" => "https://line.allcate.jp/image/image_tpl.png",
        "imageSize" => "contain",
        "title" => "ボタンTPLテスト",
        "text" => "どれか選択してください",
        "defaultAction" => array(
          "type"  => "uri",
          "label" => "外部サイト",
          "uri"   => "https://www.google.co.jp/",
        ),
        "actions" => array(
          array(
            "type"  => "postback",
            "label" => "選択肢A",
            "data"  => "from=tpl_action&selected=select_a",
          ),
          array(
            "type"  => "postback",
            "label" => "選択肢B",
            "data"  => "from=tpl_action&selected=select_b",
          ),
          array(
            "type"  => "postback",
            "label" => "選択肢C",
            "data"  => "from=tpl_action&selected=select_c",
          ),
        )
      )
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $res = $this->callApi($this->line_api_url_arr['reply_message'], 
    "POST", $addadditional_header, json_encode($send_data), true);
  }
  /**
   * リッチメニューをユーザーに紐付ける
   */
  private function setRichMenu()
  {
    $res = $this->callApi($this->line_api_url_arr['use_richmenu'], "POST");
  }
  /**
   * リッチメニューの紐付けを解除する
   */
  private function unsetRichMenu()
  {
    $res = $this->callApi($this->line_api_url_arr['stop_richmenu'], "DELETE");
  }
}

// インスタンス生成
$lineapi = new lineBotApi();
$lineapi->lineBotAction();

```

## 5. Botを友だち追加して、動作を確認する

1. QRコードから友だち追加
![QRコード](./markdown/img/friend_qr.png)
1. リッチメニュー「ボタンTPL」を押下
1. リッチメニュー「確認TPL」を押下
1. リッチメニュー「画像TPL」を押下
