<?php
/**
 * Line Messaging API を利用したテスト
 * リッチメニューは予め登録しておく
 * ユーザーへのリッチメニュー紐付けと解除をおこなう
 */
class lineBotApi
{
  private $access_token = "*****************************************"; // Line Developers管理画面上の「アクセストークン」
  private $richmenu_id  = "richmenu-********************************"; // 事前に登録したリッチメニューのID
  private $reply_token  = "";     // 返信に必要なトークン(リクエストデータに入る)
  private $user_id      = "";     // リクエストしてきたユーザーID(リクエストデータに入る)
  private $type         = "";     // リクエストのタイプ 
  private $postback_arr = array();
  private $line_api_url_arr = array();
  
  public function __construct()
  {
    // Lineからはjsonで飛んでくる
    $response_arr = json_decode(file_get_contents('php://input'), true);
    $this->log(var_export($response_arr, true));
    if(count($response_arr) == 0)
    {
      http_response_code(404);
      exit();
    }
    // 利用するメッセージの抽出
    $this->setMessages($response_arr['events'][0]);
    // 抽出したIDなどを利用してAPIのURLを設定
    $this->setUrls();
  }
  
  /**
   * 開発等でちょっとログを見たいとき用
   */
  private function log($log_str)
  {
    $fp = fopen("./lineBotApi_". date("Ymd"). ".log","a+");
    fwrite($fp, $log_str. "\n");
    fclose($fp);
  }  

  /**
   * 利用するメッセージの抽出
   * @param [array] $messages_arr [Lineからリクエストされてきたデータ]
   */
  private function setMessages($messages_arr)
  {
    $this->reply_token = $messages_arr['replyToken'];
    $this->user_id     = $messages_arr['source']['userId'];
    $this->type        = $messages_arr['type'];
    if ($messages_arr['type'] == 'postback')
    {
      parse_str($messages_arr['postback']['data'], $this->postback_arr);
    }
  }
  
  /**
   * 抽出したIDなどを利用してAPIのURLを設定
   */
  private function setUrls()
  {
    $this->line_api_url_arr = array(
      'use_richmenu'  => "https://api.line.me/v2/bot/user/{$this->user_id}/richmenu/{$this->richmenu_id}",
      'stop_richmenu' => "https://api.line.me/v2/bot/user/{$this->user_id}/richmenu",
      'reply_message' => "https://api.line.me/v2/bot/message/reply",
    );
  }
  
  /**$additional_header
   * curlを利用したAPIリクエスト
   * @param  [string] $reply_api_url     [APIのURL]
   * @param  [string] $custom_request    [POSTやDELETEなど]
   * @param  array  $additional_header [API毎に異なる必要なヘッダー]
   * @param  string $send_data         [送信データ]
   * @return [bool]
   */
  private function callApi($reply_api_url, $custom_request, $additional_header = [], $send_data = "", $return_transfer = false)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $reply_api_url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_request);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, $return_transfer);
    if($custom_request == 'POST')
    {
      curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
    }
    $header = array_merge(array("Authorization: Bearer {$this->access_token}"), $additional_header);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    return curl_exec($ch);
  }
  
  /**
   * メイン処理
   */
  public function lineBotAction()
  {
    if ($this->type == 'follow')
    {
      $this->setRichMenu();
    }
    else if ($this->type == 'unfollow')
    {
      $this->unsetRichMenu();
    }
    else if ($this->type == 'postback')
    {
      if($this->postback_arr['from'] == 'richmenu')
      {
        if ($this->postback_arr['request_type'] == 'button_tpl')
        {
          $this->sendButtonTpl();
        }
        else if ($this->postback_arr['request_type'] == 'confirm_tpl')
        {
          $this->sendConfirmTpl();
        }
        else if ($this->postback_arr['request_type'] == 'image_tpl')
        {
          $this->sendImageTpl();
        }
      }
      else if ($this->postback_arr['from'] == 'tpl_action')
      {
        $this->sendSelected();
      }
    }
  }
  private function sendImageTpl()
  {
    $reply_message[] = array(
      "type"     => "template",
      "altText" => "画像カルーセルテスト",
      "template" => array(
        "type" => "image_carousel",
        "columns" => array(
          array(
            "imageUrl" => "https://line.allcate.jp/image/yahoo.png",
            "action"   => array(
                "type"  => "uri",
                "label" => "外部リンク",
                "uri"   => "https://yahoo.co.jp"
            )
          ),
          array(
            "imageUrl" => "https://line.allcate.jp/image/line.png",
            "action"   => array(
                "type"  => "uri",
                "label" => "外部リンク",
                "uri"   => "https://line.me/ja/"
            )
          ),
        )
      )
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $res = $this->callApi($this->line_api_url_arr['reply_message'], "POST", $addadditional_header, json_encode($send_data), true);
    $this->log(var_export($res, true));
  }
  private function sendSelected()
  {
    $response_tpl_messages = array(
      'select_a'   => '「選択肢A」が選択されました',
      'select_b'   => '「選択肢B」が選択されました',
      'select_c'   => '「選択肢C」が選択されました',
      'select_yes' => '「はい」が選択されました',
      'select_no'  => '「いいえ」が選択されました',
    );

    $reply_message[] = array(
      "text" => $response_tpl_messages[$this->postback_arr['selected']],
      "type" => "text",
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $this->callApi($this->line_api_url_arr['reply_message'], "POST", $addadditional_header, json_encode($send_data));
  }
  private function sendConfirmTpl()
  {
    $reply_message[] = array(
      "type"     => "template",
      "altText" => "確認TPLテスト",
      "template" => array(
        "type" => "confirm",
        "text" => "【確認】〜してもよろしいですか？",
        "actions" => array(
          array(
            "type"  => "postback",
            "label" => "はい",
            "data"  => "from=tpl_action&selected=select_yes",
          ),
          array(
            "type"  => "postback",
            "label" => "いいえ",
            "data"  => "from=tpl_action&selected=select_no",
          ),
        )
      )
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $res = $this->callApi($this->line_api_url_arr['reply_message'], "POST", $addadditional_header, json_encode($send_data), true);
  }
  
  private function sendButtonTpl()
  {
    $reply_message[] = array(
      "type"     => "template",
      "altText" => "ボタンTPLテスト",
      "template" => array(
        "type" => "buttons",
        "thumbnailImageUrl" => "https://line.allcate.jp/image/image_tpl.png",
        "imageSize" => "contain",
        "title" => "ボタンTPLテスト",
        "text" => "どれか選択してください",
        "defaultAction" => array(
          "type"  => "uri",
          "label" => "外部サイト",
          "uri"   => "https://www.google.co.jp/",
        ),
        "actions" => array(
          array(
            "type"  => "postback",
            "label" => "選択肢A",
            "data"  => "from=tpl_action&selected=select_a",
          ),
          array(
            "type"  => "postback",
            "label" => "選択肢B",
            "data"  => "from=tpl_action&selected=select_b",
          ),
          array(
            "type"  => "postback",
            "label" => "選択肢C",
            "data"  => "from=tpl_action&selected=select_c",
          ),
        )
      )
    );
    $send_data = array(
      'replyToken' => $this->reply_token,
      'messages'   => $reply_message,
    );
    $addadditional_header = array("Content-Type: application/json", );
    $res = $this->callApi($this->line_api_url_arr['reply_message'], "POST", $addadditional_header, json_encode($send_data), true);
  }
  private function setRichMenu()
  {
    $res = $this->callApi($this->line_api_url_arr['use_richmenu'], "POST");
    return ($res == true) ?  "リッチメニューの設定が完了しました" : "リッチメニューの設定に失敗しました";
  }
  private function unsetRichMenu()
  {
    $res = $this->callApi($this->line_api_url_arr['stop_richmenu'], "DELETE");
    return ($res == true) ?  "リッチメニューの解除が完了しました" : "リッチメニューの解除に失敗しました";
  }
}

// インスタンス生成
$lineapi = new lineBotApi();
$lineapi->lineBotAction();
